# README #

Version 1.0
Este es un proyecto de demostración para el examen técnico de lagash

El proyecto se encuentra organizado de la siguiente manera:

Assets
 Escenas (aqui se encuentran las escenas de Unity)
 Materials (materiales y texturas)
 Models (modelos 3D, Texturas y prefabs)
 Prefabs (plantillas de escenario, Player, Enemigos)
 Scripts (Fragmentos de codigo para el comportamiento del sistema)
 
 Cuenta con un singleton GameManager.cs que controla los estados de juego
 El movimiento del vehiculo se implemento con física, se considera el torque y la potencia del motor para realizar el movimiento
 
 El objetivo del juego es tomar el mayor número de checkpoints antes de que se termine el tiempo, cada que el auto rival toma un checkpoint
 se reduce tu tiempo en 10 segundos, cada que tomas un checkpoint ganas 10 segundos, si tu auto se voltea puedes presionar enter para
 regresarlo a la posición original.
 
 Puedes elegir entre 2 tipos de auto diferentes.
 Presiona Escape en cualquier momento dentro del juego para hacer una pausa.
 El enemigo utiliza un navMesh para localizar el siguiente Checkpoint.
 Se utilizó Occlusion Culling para optimizar el rendereado del escenario.

Controles UpArrow - Acelerar
          DownArrow - Freno/Marcha Atras
          LeftArrow - Girar Izquierda
          RigthArrow - Girar Derecha

 
 Elaborado por Luis Moisés Sandoval Escobedo
 luis.sandoval.escobedo@hotmail.com
 http://www.luissandoval.com.mx