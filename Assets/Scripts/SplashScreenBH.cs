﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScreenBH : MonoBehaviour
{
    float time;
    float timeMax;

    GameManagerBH gameManager;

    void Awake()
    {
        gameManager = GameManagerBH.Instance;
    }

    void Start()
    {
        gameManager.OnStateChange += HandleOnStateChange;
        timeMax = 5.0f;
        gameManager.SetGameState(GameState.SplashScreen);
    }

    void Update()
    {
        time += Time.deltaTime;

        if (time > timeMax)
        {
            SceneManager.LoadScene("MainMenu");
            gameManager.SetGameState(GameState.MainMenu);
        }
    }

    public void HandleOnStateChange()
    {
        Debug.Log("Estoy en el estado: " + gameManager.gameState);
    }
}
