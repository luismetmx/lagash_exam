﻿using UnityEngine;

public enum GameState
{ SplashScreen, MainMenu, GamePlay, GameOver, Pausa }

public delegate void OnStateChangeHandler();

public class GameManagerBH : MonoBehaviour
{
    private static GameManagerBH instance = null;
    public event OnStateChangeHandler OnStateChange;
    public GameState gameState { get; private set; }

    protected GameManagerBH()
    {

    }

    public static GameManagerBH Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new GameManagerBH();
                DontDestroyOnLoad(instance);
            }
            return instance;
        }

    }

    public void SetGameState(GameState state)
    {
        gameState = state;
        OnStateChange();
    }

    public void OnApplicationQuit()
    {
        instance = null;
    }

}