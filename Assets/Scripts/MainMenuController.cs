﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    public static int CarId;
    GameManagerBH gameManager;

    void Awake()
    {
        gameManager = GameManagerBH.Instance;

    }

    // Use this for initialization
    void Start()
    {
        CarId = 0;
        gameManager.OnStateChange += HandleOnStateChange;
        gameManager.SetGameState(GameState.MainMenu);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void HandleOnStateChange()
    {
        Debug.Log("Estoy en el estado: " + gameManager.gameState);

        if(gameManager.gameState == GameState.MainMenu)
        {
            Time.timeScale = 1;
        }
    }

    public void OnCarSelected(int carSelectedID)
    {
        CarId = carSelectedID;
        SceneManager.LoadScene("GamePlay");
    }
}

