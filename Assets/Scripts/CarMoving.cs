﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class CarMoving : MonoBehaviour
{
    //CarMovingControl

    public Transform[] wheels;  //Posicion de las ruedas
    public float enginePower = 150.0f; //Maxima potencia del motor

    float power = 0.0f;
    float brake = 0.0f;
    float steer = 0.0f;
    public float maxSteer = 45.0f; //Angulo Máximo de giro para las llantas
    public Rigidbody rigid;
    public Transform centroMasa;
    public static Text TenSecondsWin;
    public static string textForPlayer = "";
    public GameObject Flecha;
    float time;

    void Start()
    {
        rigid.centerOfMass = centroMasa.position;
        time = 0.0f;
        TenSecondsWin = GetComponentInChildren<Text>();
        TenSecondsWin.enabled = false;
    }

    void Update()
    {
        power = -Input.GetAxis("Vertical") * enginePower * Time.deltaTime * 250.0f;
        steer = Input.GetAxis("Horizontal") * maxSteer;
        brake = Input.GetKey(KeyCode.Space) ? rigid.mass * 0.1f : 0.0f;

        GetCollider(0).steerAngle = steer;
        GetCollider(1).steerAngle = steer;

        if (brake > 0.0)
        {
            GetCollider(0).brakeTorque = brake;
            GetCollider(1).brakeTorque = brake;
            GetCollider(2).brakeTorque = brake;
            GetCollider(3).brakeTorque = brake;
            GetCollider(2).motorTorque = 0.0f;
            GetCollider(3).motorTorque = 0.0f;
        }
        else
        {
            GetCollider(0).brakeTorque = 0;
            GetCollider(1).brakeTorque = 0;
            GetCollider(2).brakeTorque = 0;
            GetCollider(3).brakeTorque = 0;
            GetCollider(2).motorTorque = power;
            GetCollider(3).motorTorque = power;
        }

        if(TenSecondsWin.enabled)
        {
            TenSecondsWin.text = textForPlayer;
            time += Time.deltaTime;
            if (time >= 5.0f)
            {
                time = 0;
                TenSecondsWin.enabled = false;
            }
        }
        
        if(Input.GetKeyDown(KeyCode.Return))
        {
            transform.position = new Vector3(0, 0.5f, 0);
        }

        Flecha.transform.LookAt(GamePlayBehavior.currentSpawnPoint.transform.position);
    }

    public WheelCollider GetCollider(int n)
    {
        return wheels[n].gameObject.GetComponent<WheelCollider>();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "CheckPoint")
        {

            other.gameObject.SetActive(false);
            GamePlayBehavior.nextPoint = true;
            GamePlayBehavior.points++;
            GamePlayBehavior.timeMax = GamePlayBehavior.timeMax + 10;
            TenSecondsWin.enabled = true;
            textForPlayer = "Ganaste 10 segundos mas";
            //navMeshAgent.destination = navMeshAgent.destination = GamePlayBehavior.currentSpawnPoint.transform.position;
            Debug.Log("El player llego al destino");
        }
    }
}
