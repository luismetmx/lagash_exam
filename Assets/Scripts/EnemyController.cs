﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    NavMeshAgent navMeshAgent;
    // Use this for initialization
    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        navMeshAgent.destination = GamePlayBehavior.currentSpawnPoint.transform.position;
        //transform.LookAt(GamePlayBehavior.currentSpawnPoint.transform);
    }

    public void OnTriggerEnter(Collider other)
    {
        if(other.tag == "CheckPoint")
        {

            other.gameObject.SetActive(false);
            GamePlayBehavior.nextPoint = true;
            CarMoving.textForPlayer = "El enemigo llego al destino pierdes 10 segundos";
            CarMoving.TenSecondsWin.enabled = true;
            GamePlayBehavior.timeMax = GamePlayBehavior.timeMax - 10;
            //navMeshAgent.destination = navMeshAgent.destination = GamePlayBehavior.currentSpawnPoint.transform.position;
            Debug.Log("El enemigo llego al destino");
        }
    }

}
