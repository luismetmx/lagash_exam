﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GamePlayBehavior : MonoBehaviour
{
    GameManagerBH gameManager;
    public GameObject[] CarType;
    public Canvas PausaCanvas;
    public Canvas GameOverCanvas;
    public static float timeMax;
    public static int points;
    public Text timeLeft;
    public Text pointsText;
    public GameObject [] SpawnPoints;
    public static GameObject currentSpawnPoint;
    public static bool nextPoint;

    void Awake()
    {
        gameManager = GameManagerBH.Instance;
    }

    void Start()
    {
        gameManager.OnStateChange += HandleOnStateChange;
        gameManager.SetGameState(GameState.GamePlay);
        Instantiate(CarType[MainMenuController.CarId], new Vector3(0, 0.5f, 0), Quaternion.identity);
        timeMax = 90.0f;
        points = 0;
        nextPoint = false;
        currentSpawnPoint = SpawnPoints[Random.Range(0,5)];
        currentSpawnPoint.SetActive(true);
    }

    public void OnReanudar()
    {
        if(gameManager.gameState == GameState.Pausa)
        {
            gameManager.SetGameState(GameState.GamePlay);
        }
    }
    public void OnSalir()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void OnReStart()
    {
        SceneManager.LoadScene("Gameplay");
    }

    public void HandleOnStateChange()
    {
        Debug.Log("Estoy en el estado: " + gameManager.gameState);

        if (gameManager.gameState == GameState.Pausa)
        {
            GameOverCanvas.enabled = false;
            PausaCanvas.enabled = true;
            Time.timeScale = 0;
        }

        if(gameManager.gameState == GameState.GamePlay)
        {
            GameOverCanvas.enabled = false;
            PausaCanvas.enabled = false;
            Time.timeScale = 1;
        }

        if(gameManager.gameState == GameState.GameOver)
        {
            GameOverCanvas.enabled = true;
            PausaCanvas.enabled = false;
            Time.timeScale = 0;
        }
    }

    public void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            gameManager.SetGameState(GameState.Pausa);
        }

        timeMax -= Time.deltaTime;
        timeLeft.text = "Time Left: " + timeMax.ToString("0");
        pointsText.text = "Points: " + points.ToString();

        if(timeMax <= 0)
        {
            timeMax = 0;
            gameManager.SetGameState(GameState.GameOver);
        }

        if(nextPoint)
        {
            nextPoint = false;
            currentSpawnPoint = SpawnPoints[Random.Range(0, 5)];
            currentSpawnPoint.SetActive(true);
        }
    }
}
